'user strict'

var io = require('socket.io');
var usernames = {};

exports.initialize = function(server) {
    io = io.listen(server);
    io.sockets.on('connection', function(socket){

        socket.on('message', function(message){
            message = JSON.parse(message);
            if (message.type == "userMessage") {
                message.type = 'myMessage';
                message.username = usernames[socket.id];
                socket.broadcast.send(JSON.stringify(message));
                socket.send(JSON.stringify(message));
            }
            if (message.type == "adminMessage"){
                message.type = 'adminMessage';
                message.username ='Admin';
                socket.broadcast.send(JSON.stringify(message));
                socket.send(JSON.stringify(message));
            }
        });

        socket.on("set_name", function(data){
            socket.emit('readyChat', data);
            usernames[socket.id] = data.name;
            socket.send(JSON.stringify({type:'serverMessage',
                message: 'Welcome to the most interesting chat room on earth!'}));
        });

    })
}
