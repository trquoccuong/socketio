'user strict'


var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var sessionStore = new RedisStore();
var cookieParser = require('cookie-parser');

var uuid = require('node-uuid');

var io = require('socket.io');

var usernames = {};
var admin = {};
var idAdmin;
var currentChatID;
var chatLog = {};
var nameAdmin = 'Admin';


exports.initialize = function(server) {

    io = io(server);

    io.on('connection', function(socket){
        //console.log(socket.request);
        //console.log(socket.id);
        //Quan ly he thong tin nhan
        /*
         * Khi tin nhan den check xem admin co online hay ko
         * Sau do check type cua user hay cua admin
         * neu cua user send tin nhan den thang admin
         * neu cua admin send tin nhan den thang current chat
         * */
        socket.on('message', function(message){
            message = JSON.parse(message);
            if (idAdmin && message.message != "")  {
                if (message.type == "userMessage") {

                    message.username = usernames[message.id]['name'];
                    chatLog[message.id].push(message);
                    io.to(usernames[message.id]['arrayID'][0]).emit('message',JSON.stringify(message));

                    if (currentChatID == message.id) {

                        io.to(admin[idAdmin][0]).emit('message',JSON.stringify(message));

                    } else {
                        usernames[message.id]['numberMessage']++;
                        usernames[message.id]['showNotify'] = 'showNotify';
                        io.to(admin[idAdmin][0]).emit('listUser',JSON.stringify(usernames));
                    }
                }
                if (message.type == "adminMessage"){
                    message.username = nameAdmin;
                    io.to(admin[idAdmin][0]).emit('message',JSON.stringify(message));
                    if (currentChatID) {
                        chatLog[currentChatID].push(message);
                        io.to(usernames[message.id]['arrayID'][0]).emit('message',JSON.stringify(message));
                    }
                }
            } else if (message.message != "") {
                if (message.type == "userMessage") {
                    message.username = usernames[message.id]['name'];
                    chatLog[message.id].push(message);
                    io.to(usernames[message.id]['arrayID'][0]).emit('message', JSON.stringify(message));
                }
            }
        });


        socket.on('disconnect', function(){
            if (socket.id == admin) {
                admin = false;
            } else {
                if(usernames[socket.id]) {
                    usernames[socket.id]['userState'] = 'offline';
                    io.to(admin[idAdmin][0]).emit('listUser',JSON.stringify(usernames));
                    var message = usernames[socket.id]['name'] + ' disconnected';
                    io.to(admin[idAdmin][0]).emit('message', JSON.stringify({type: 'systemMessage', message: message}));
                }
            }

        })


        // Admin emit
        socket.on("startSupport",function(data){
            idAdmin = uuid.v1();
            admin[idAdmin] = [];
            admin[idAdmin].push(data.id);
            io.to(data.id).emit('setupAdmin',idAdmin);
            io.to(data.id).emit('listUser',JSON.stringify(usernames));
        });

        socket.on("changeUser", function(data){
            currentChatID = data.userID;

            usernames[data.userID]['numberMessage'] = 0;
            usernames[data.userID]['showNotify'] = 'hideNotify';

            io.to(admin[idAdmin][0]).emit('listUser',JSON.stringify(usernames));

            io.to(admin[idAdmin][0]).emit("getLogChatByUser",JSON.stringify(chatLog[data.userID]));

            var message =  "Start chat with " + usernames[data.userID]['name'];
            io.to(admin[idAdmin][0]).emit('message',JSON.stringify({type: 'systemMessage', message: message}));

        });


        // User emit
        socket.on("set_name", function(data){
            var id = uuid.v1();

            socket.emit('readyChat', JSON.stringify({data : data, id: id}));

            usernames[id] = {};
            usernames[id]['arrayID'] = [];
            usernames[id]['arrayID'].push(socket.id);
            usernames[id]['name'] = data.name;
            usernames[id]['numberMessage'] = 0;
            usernames[id]['userState'] = 'online';
            usernames[id]['showNotify'] = 'hideNotify';

            chatLog[id] = [];

            if (idAdmin) {
                socket.send(JSON.stringify({type:'systemMessage',
                    message: 'Welcome to the most interesting chat room on earth!'}));
            } else {
                socket.send(JSON.stringify({type:'systemMessage',
                    message: 'No admin online'}));
            }
            // make notify for admin
            var message = data.name + ' connected';
            io.to(admin[idAdmin][0]).emit('message',JSON.stringify({ type: 'systemMessage', message: message}));
            io.to(admin[idAdmin][0]).emit('listUser',JSON.stringify(usernames));
        });
    })
}
