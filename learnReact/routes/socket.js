'use strict'

var io = require('socket.io');
var cookieParser = require("cookie-parser");

var usernames = {};
var adminID;

exports.init = function(server) {
    io = io.listen(server)


    var admin = io.of('/admin');
    admin.on('connection',function(socket){
        adminID = true;
        console.log('admin connected');

        socket.on('disconnected',function(){
            adminID = false;
        })
        socket.on('logout',function(){
            adminID = false;

        })
    })


    var nsp = io.of('/user');
    nsp.on('connection', function(socket){

        console.log('user connected');

    });


    var demo = io.of('/demo');
    demo.on('connection', function(socket){
        socket.on('message', function(message){

            message = JSON.parse(message);
            message.username = usernames[socket.id];

            demo.emit('message',JSON.stringify(message))
        });

        socket.on('setName',function(data){
            usernames[socket.id] = data.nickname;
        });

    })


    //io.on('connection', function(socket){
    //
    //    socket.on("set_name", function(data){
    //        socket.emit('readyChat', data);
    //        usernames[socket.id] = data.name;
    //        socket.send(JSON.stringify({type:'serverMessage',
    //            message: 'Welcome to the most interesting chat room on earth!'}));
    //    });
    //
    //
    //    socket.on('message', function(message){
    //        message = JSON.parse(message);
    //        if (message.type == "userMessage") {
    //            socket.broadcast.send(JSON.stringify(message));
    //            message.type = 'myMessage';
    //            message.username = usernames[socket.id];
    //            socket.send(JSON.stringify(message));
    //        }
    //        if (message.type == "adminMessage"){
    //            socket.broadcast.send(JSON.stringify(message));
    //            message.type = 'adminMessage';
    //            message.username ='Admin';
    //            socket.send(JSON.stringify(message));
    //        }
    //    });
    //
    //    socket.on('disconnect', function(){ });
    //})
}