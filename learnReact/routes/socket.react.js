'user strict'


var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var sessionStore = new RedisStore();
var cookieParser = require('cookie-parser');

var uuid = require('node-uuid');

var io = require('socket.io');

var usernames = {};
var adminID ;
var currentChatID;
var chatLog = {};
var nameAdmin = 'Admin';


exports.initialize = function(server) {

    io = io(server);

    io.on('connection', function(socket){
        console.log(socket.request);
        console.log(socket.id);
        //Quan ly he thong tin nhan
        /*
         * Khi tin nhan den check xem admin co online hay ko
         * Sau do check type cua user hay cua admin
         * neu cua user send tin nhan den thang admin
         * neu cua admin send tin nhan den thang current chat
         * */
        socket.on('message', function(message){
            message = JSON.parse(message);
            if (adminID && message.message != "")  {
                if (message.type == "userMessage") {

                    message.username = usernames[socket.id]['name'];
                    chatLog[socket.id].push(message);
                    io.to(socket.id).emit('message',JSON.stringify(message));

                    if (currentChatID == socket.id) {

                        io.to(adminID).emit('message',JSON.stringify(message));

                    } else {
                        usernames[socket.id]['numberMessage']++;
                        usernames[socket.id]['showNotify'] = 'showNotify';
                        io.to(adminID).emit('listUser',JSON.stringify(usernames));
                    }
                }
                if (message.type == "adminMessage"){
                    message.username =nameAdmin;
                    io.to(socket.id).emit('message',JSON.stringify(message));
                    if (currentChatID) {
                        chatLog[currentChatID].push(message);
                        io.to(currentChatID).emit('message',JSON.stringify(message));
                    }
                }
            } else if (message.message != "") {
                if (message.type == "userMessage") {
                    message.username = usernames[socket.id]['name'];
                    chatLog[socket.id].push(message);
                    io.to(socket.id).emit('message', JSON.stringify(message));
                }
            }
        });


        socket.on('disconnect', function(){
            if (socket.id == adminID) {
                adminID = false;
            } else {
                if(usernames[socket.id]) {
                    usernames[socket.id]['userState'] = 'offline';
                    io.to(adminID).emit('listUser',JSON.stringify(usernames));
                    var message = usernames[socket.id]['name'] + ' disconnected';
                    io.to(adminID).emit('message', JSON.stringify({type: 'systemMessage', message: message}));
                }
            }

        })


        // Admin emit
        socket.on("startSupport",function(data){
            adminID = data.id;
            io.to(adminID).emit('listUser',JSON.stringify(usernames));
        });

        socket.on("changeUser", function(data){
            currentChatID = data.userID;

            usernames[currentChatID]['numberMessage'] = 0;
            usernames[currentChatID]['showNotify'] = 'hideNotify';

            io.to(adminID).emit('listUser',JSON.stringify(usernames));

            io.to(adminID).emit("getLogChatByUser",JSON.stringify(chatLog[currentChatID]));

            var message =  "Start chat with " + usernames[currentChatID]['name'];
            io.to(adminID).emit('message',JSON.stringify({type: 'systemMessage', message: message}));

        });


        // User emit
        socket.on("set_name", function(data){
            socket.emit('readyChat', data);
            usernames[socket.id] = {};
            usernames[socket.id]['name'] = data.name;
            usernames[socket.id]['numberMessage'] = 0;
            usernames[socket.id]['userState'] = 'online';
            usernames[socket.id]['showNotify'] = 'hideNotify';
            chatLog[socket.id] = [];

            if (adminID) {
                socket.send(JSON.stringify({type:'systemMessage',
                    message: 'Welcome to the most interesting chat room on earth!'}));
            } else {
                socket.send(JSON.stringify({type:'systemMessage',
                    message: 'No admin online'}));
            }
            console.log(usernames);
            // make notify for admin
            var message = data.name + ' connected';
            io.to(adminID).emit('message',JSON.stringify({ type: 'systemMessage', message: message}));
            io.to(adminID).emit('listUser',JSON.stringify(usernames));
        });
    })
}
