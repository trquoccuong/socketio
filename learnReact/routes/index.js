var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/react', function(req,res){
  res.render('react/hello');
})

router.get('/chat/client', function(req,res) {
  res.render('chat/client');
})
router.get('/chat/client-demo', function(req,res) {
  res.render('chat/client-test');
})

router.get('/chat/server', function(req,res) {
  res.render('chat/server');
})

router.get('/chat/server-demo', function(req,res) {
  res.render('chat/server-test');
})

router.get('/chat/demo', function(req,res) {
  res.render('chat/demo');
})

module.exports = router;
