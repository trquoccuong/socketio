'user strict'

var io = require('socket.io');
var usernames = {};
var currentChat;
var adminID ;

exports.initialize = function(server) {
    io = io.listen(server);
    io.sockets.on('connection', function(socket){
        //socket.on('support', function(data){
        //    adminID = data;
        //})

        socket.on('message', function(message){
            message = JSON.parse(message);
            if (message.type == "userMessage") {
                socket.broadcast.send(JSON.stringify(message));
                message.type = 'myMessage';
                message.username = usernames[socket.id];
                socket.send(JSON.stringify(message));
            }
            if (message.type == "adminMessage"){
                socket.broadcast.to(currentChat).send(JSON.stringify(message));
                message.type = 'adminMessage';
                message.username ='Admin';
                socket.send(JSON.stringify(message));
            }
        });

        socket.on("set_name", function(data){
            socket.emit('readyChat', data);
            usernames[socket.id] = data.name;
            socket.emit('newcomer', usernames);
            socket.send(JSON.stringify({type:'serverMessage',
                message: 'Welcome to the most interesting chat room on earth!'}));
        });

        socket.on("joinRoom", function(data){
            console.log(data);
        })

    })
}
